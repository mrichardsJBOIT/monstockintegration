package com.monstock.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 * Camel as client that calls the hello service using a timer every 2nd seconds and logs the response
 */
@Component
public class ContractsRoute extends RouteBuilder {

	@Override
    public void configure() throws Exception {
        from("timer:foo?repeatCount=1")
                .to("https4://api.cloudbeta.monstock.net/api/v1/integration-bot")
                .log("${body}")
                .to("seda:contracts");
    }
}
