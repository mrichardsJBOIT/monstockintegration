package com.monstock.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import com.monstock.beans.MonstockProduct;

/**
 * Camel as client that calls the hello service using a timer every 2nd seconds and logs the response
 */
@Component
public class ArticlesRoute extends RouteBuilder {

	@Override
    public void configure() throws Exception {
        from("timer:foo?period=2000&repeatCount=1")
                .to("http4://demo.restomax.com:8787/action?action=Resto:GetArticles")
                .split().jsonpath("$")
                .streaming().marshal().json(JsonLibrary.Jackson, true).log("${body}").
                to("seda:articles");
                
//        from("seda:articles")
//    	.choice()
//    		.when().jsonpath("$..[?(@.PLU =~ /^[0-9]{4}/)]")
//    			.setHeader("plu").jsonpath("$.PLU").to("file://target/articles/NumericPLU?fileName=PLU-${in.header.plu}.json")
//    		.otherwise()
//    			.setHeader("plu").jsonpath("$.PLU").to("file://target/articles/nonNumericPLU?fileName=PLU-${in.header.plu}.json");
        
        
        from("seda:articles")
        	.choice()
        		.when().jsonpath("$..[?(@.PLU =~ /^[0-9]{4}/)]",true)
        			.to("seda:productsNumericPLU")
        		.otherwise()
        			.to ("seda:productsNonNumericPLU");
        
        from ("seda:productsWithNonNumericPLU")
    		.setHeader("plu").jsonpath("$.PLU",true)
    			.to("file://target/articles/nonNumericPLU?fileName=PLU-${in.header.plu}.json");
        
        from("seda:productsNumericPLU")
        	.choice()	
        		.when().jsonpath("$..[?(@.DetailsPrepa.length() > 0)]",true)
        			.to ("seda:productsWithChildren")
        		.otherwise()
        			.to ("seda:productsWithNoChildren");
        
        from ("seda:productsWithChildren")
        	.bean(new MonstockProduct())
        	.setHeader("plu").jsonpath("$.PLU",true)
        		.to("file://target/articles/hasChildren?fileName=PLU-${in.header.plu}.json");
		
        from("seda:productsWithNoChildren")
        	.bean(new MonstockProduct())
        	.setHeader("id").jsonpath("$.id")
        		.to("file://target/products/?fileName=MSP-${in.header.id}.json");        
    }
}
