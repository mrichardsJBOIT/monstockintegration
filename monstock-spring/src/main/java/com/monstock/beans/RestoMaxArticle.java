package com.monstock.beans;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY )
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RestoMaxArticle {

	static class IR5BtnProperties {
		private String Color;
		private String FontColor;
		private Integer FontSize;
		private String Font;
		private int Bmp;
		private int BevelWidth;
		private int Properties;
		private String BtnStyle;
		private String TextStyle;
		
		public String getColor() {
			return Color;
		}
		public void setColor(String color) {
			Color = color;
		}
		public String getFontColor() {
			return FontColor;
		}
		public void setFontColor(String fontColor) {
			FontColor = fontColor;
		}
		public Integer getFontSize() {
			return FontSize;
		}
		public void setFontSize(Integer fontSize) {
			FontSize = fontSize;
		}
		public String getFont() {
			return Font;
		}
		public void setFont(String font) {
			Font = font;
		}
		public int getBmp() {
			return Bmp;
		}
		public void setBmp(int bmp) {
			Bmp = bmp;
		}
		public int getBevelWidth() {
			return BevelWidth;
		}
		public void setBevelWidth(int bevelWidth) {
			BevelWidth = bevelWidth;
		}
		public int getProperties() {
			return Properties;
		}
		public void setProperties(int properties) {
			Properties = properties;
		}
		public String getBtnStyle() {
			return BtnStyle;
		}
		public void setBtnStyle(String btnStyle) {
			BtnStyle = btnStyle;
		}
		public String getTextStyle() {
			return TextStyle;
		}
		public void setTextStyle(String textStyle) {
			TextStyle = textStyle;
		}
		public String getBtnSize() {
			return BtnSize;
		}
		public void setBtnSize(String btnSize) {
			BtnSize = btnSize;
		}
		private String BtnSize;
	}

	static class IDetailStock {
		private String PLU;
		private String Qty;
		public String getPLU() {
			return PLU;
		}
		public void setPLU(String pLU) {
			PLU = pLU;
		}
		public String getQty() {
			return Qty;
		}
		public void setQty(String qty) {
			Qty = qty;
		}
	}

	static class IDetailPrepa {
		private String PLU;
		private String Niveau;
		public String getPLU() {
			return PLU;
		}
		public void setPLU(String pLU) {
			PLU = pLU;
		}
		public String getNiveau() {
			return Niveau;
		}
		public void setNiveau(String niveau) {
			Niveau = niveau;
		}
		
	}

	static class IDetailMenu {
		private String PLU;
		private String Niveau;
		private String PxIn;
		private String PxOut;
		public String getPLU() {
			return PLU;
		}
		public void setPLU(String pLU) {
			PLU = pLU;
		}
		public String getNiveau() {
			return Niveau;
		}
		public void setNiveau(String niveau) {
			Niveau = niveau;
		}
		public String getPxIn() {
			return PxIn;
		}
		public void setPxIn(String pxIn) {
			PxIn = pxIn;
		}
		public String getPxOut() {
			return PxOut;
		}
		public void setPxOut(String pxOut) {
			PxOut = pxOut;
		}
		
	}

	private String PLU;
	private String Categorie;
	private String Nom;
	private int Type;
	private String TypeDescr;
	private int Flags;
	private int Allergens;
	private String Action;
	private String NomAddFR;
	private String NomAddNL;
	private String NomAddUK;
	private String NomProdFR;
	private String BtnTextFR;
	private IR5BtnProperties BtnProp;
	private String MemoCuisine;
	private String Barcode;
	private int Desti;
	private int GotoMode;
	private int GotoOnglet;
	private int CategPos;
	private int Groupe;
	private String Divisions;
	private int TVA;
	private String TVACoeff;
	private int TVAEmport;
	private String TVAEmportCoeff;
	private int PrixAchat;
	private int PrixAchatEmport;
	private int Prix1;
	private int Prix2;
	private int Prix3;
	private int Prix4;
	private int Prix1HappyHour;
	private int Prix2HappyHour;
	private int Prix3HappyHour;
	private int Prix4HappyHour;
	private String Unit;
	private int Packaging;
	private int Tare;
	private int PrimeGarcon;
	private String[] DetailsAuto;
	private IDetailPrepa[] DetailsPrepa;
	private IDetailMenu[] DetailsMenu;
	private IDetailStock[] DetailsStock;
	private String CreationDate;
	private String LastModifDate;
	private int LastModifStaff;
	private boolean Bloque;
	private boolean Pointe;
	private boolean Disabled;
	private boolean PrixASaissir;
	private boolean NomASaissir;
	private boolean OnlyResto;
	private boolean OnlyEmport;
	private boolean ForceEmport;
	private boolean PointageNoCumul;
	private boolean ForceResto;
	private boolean InterditAZero;
	private boolean InterditHorsMenu;
	private boolean InterditDansMenu;
	private boolean OnlyOnceRemise;
	private boolean HappyHourQty;
	private boolean NotInTotalDiscount;
	private boolean NoChgPrix;
	private boolean NonFood;
	private boolean ControlINOUT;
	private boolean MenuForfaitCandidat;
	private boolean KeepStock;
	private boolean ListeReassort;
	private boolean BlocageAuto;
	private boolean AuCentimetre;
	private boolean NoDetailsIfResto;
	private boolean NoDetailsIfEmport;
	private boolean OnlyManager;
	private boolean Balance;
	private boolean External;
	private boolean AdditionNoCumul;
	private boolean NoTicketIfEmport;
	private boolean KeepStockOnlyIngredients;
	private boolean StayInDepartement;
	private boolean ForceDetailLinked;
	private boolean PreparationOpen;
	private boolean PreparationControled;
	private boolean PreparationGrouped;
	private boolean PreparationShowNivDescr;
	private boolean PreparationSortByNom;
	private boolean NeedDrapeau4Lancer;
	private boolean NeedDrapeau4Add;
	private boolean RptModifFlag1;
	private boolean RptModifFlag2;
	private boolean RptModifFlag3;
	private boolean RptModifFlag4;
	public String getPLU() {
		return PLU;
	}
	public void setPLU(String pLU) {
		PLU = pLU;
	}
	public String getCategorie() {
		return Categorie;
	}
	public void setCategorie(String categorie) {
		Categorie = categorie;
	}
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public int getType() {
		return Type;
	}
	public void setType(int type) {
		Type = type;
	}
	public String getTypeDescr() {
		return TypeDescr;
	}
	public void setTypeDescr(String typeDescr) {
		TypeDescr = typeDescr;
	}
	public int getFlags() {
		return Flags;
	}
	public void setFlags(int flags) {
		Flags = flags;
	}
	public int getAllergens() {
		return Allergens;
	}
	public void setAllergens(int allergens) {
		Allergens = allergens;
	}
	public String getAction() {
		return Action;
	}
	public void setAction(String action) {
		Action = action;
	}
	public String getNomAddFR() {
		return NomAddFR;
	}
	public void setNomAddFR(String nomAddFR) {
		NomAddFR = nomAddFR;
	}
	public String getNomAddNL() {
		return NomAddNL;
	}
	public void setNomAddNL(String nomAddNL) {
		NomAddNL = nomAddNL;
	}
	public String getNomAddUK() {
		return NomAddUK;
	}
	public void setNomAddUK(String nomAddUK) {
		NomAddUK = nomAddUK;
	}
	public String getNomProdFR() {
		return NomProdFR;
	}
	public void setNomProdFR(String nomProdFR) {
		NomProdFR = nomProdFR;
	}
	public String getBtnTextFR() {
		return BtnTextFR;
	}
	public void setBtnTextFR(String btnTextFR) {
		BtnTextFR = btnTextFR;
	}
	public IR5BtnProperties getBtnProp() {
		return BtnProp;
	}
	public void setBtnProp(IR5BtnProperties btnProp) {
		BtnProp = btnProp;
	}
	public String getMemoCuisine() {
		return MemoCuisine;
	}
	public void setMemoCuisine(String memoCuisine) {
		MemoCuisine = memoCuisine;
	}
	public String getBarcode() {
		return Barcode;
	}
	public void setBarcode(String barcode) {
		Barcode = barcode;
	}
	public int getDesti() {
		return Desti;
	}
	public void setDesti(int desti) {
		Desti = desti;
	}
	public int getGotoMode() {
		return GotoMode;
	}
	public void setGotoMode(int gotoMode) {
		GotoMode = gotoMode;
	}
	public int getGotoOnglet() {
		return GotoOnglet;
	}
	public void setGotoOnglet(int gotoOnglet) {
		GotoOnglet = gotoOnglet;
	}
	public int getCategPos() {
		return CategPos;
	}
	public void setCategPos(int categPos) {
		CategPos = categPos;
	}
	public int getGroupe() {
		return Groupe;
	}
	public void setGroupe(int groupe) {
		Groupe = groupe;
	}
	public String getDivisions() {
		return Divisions;
	}
	public void setDivisions(String divisions) {
		Divisions = divisions;
	}
	public int getTVA() {
		return TVA;
	}
	public void setTVA(int tVA) {
		TVA = tVA;
	}
	public String getTVACoeff() {
		return TVACoeff;
	}
	public void setTVACoeff(String tVACoeff) {
		TVACoeff = tVACoeff;
	}
	public int getTVAEmport() {
		return TVAEmport;
	}
	public void setTVAEmport(int tVAEmport) {
		TVAEmport = tVAEmport;
	}
	public String getTVAEmportCoeff() {
		return TVAEmportCoeff;
	}
	public void setTVAEmportCoeff(String tVAEmportCoeff) {
		TVAEmportCoeff = tVAEmportCoeff;
	}
	public int getPrixAchat() {
		return PrixAchat;
	}
	public void setPrixAchat(int prixAchat) {
		PrixAchat = prixAchat;
	}
	public int getPrixAchatEmport() {
		return PrixAchatEmport;
	}
	public void setPrixAchatEmport(int prixAchatEmport) {
		PrixAchatEmport = prixAchatEmport;
	}
	public int getPrix1() {
		return Prix1;
	}
	public void setPrix1(int prix1) {
		Prix1 = prix1;
	}
	public int getPrix2() {
		return Prix2;
	}
	public void setPrix2(int prix2) {
		Prix2 = prix2;
	}
	public int getPrix3() {
		return Prix3;
	}
	public void setPrix3(int prix3) {
		Prix3 = prix3;
	}
	public int getPrix4() {
		return Prix4;
	}
	public void setPrix4(int prix4) {
		Prix4 = prix4;
	}
	public int getPrix1HappyHour() {
		return Prix1HappyHour;
	}
	public void setPrix1HappyHour(int prix1HappyHour) {
		Prix1HappyHour = prix1HappyHour;
	}
	public int getPrix2HappyHour() {
		return Prix2HappyHour;
	}
	public void setPrix2HappyHour(int prix2HappyHour) {
		Prix2HappyHour = prix2HappyHour;
	}
	public int getPrix3HappyHour() {
		return Prix3HappyHour;
	}
	public void setPrix3HappyHour(int prix3HappyHour) {
		Prix3HappyHour = prix3HappyHour;
	}
	public int getPrix4HappyHour() {
		return Prix4HappyHour;
	}
	public void setPrix4HappyHour(int prix4HappyHour) {
		Prix4HappyHour = prix4HappyHour;
	}
	public String getUnit() {
		return Unit;
	}
	public void setUnit(String unit) {
		Unit = unit;
	}
	public int getPackaging() {
		return Packaging;
	}
	public void setPackaging(int packaging) {
		Packaging = packaging;
	}
	public int getTare() {
		return Tare;
	}
	public void setTare(int tare) {
		Tare = tare;
	}
	public int getPrimeGarcon() {
		return PrimeGarcon;
	}
	public void setPrimeGarcon(int primeGarcon) {
		PrimeGarcon = primeGarcon;
	}
	public String[] getDetailsAuto() {
		return DetailsAuto;
	}
	public void setDetailsAuto(String[] detailsAuto) {
		DetailsAuto = detailsAuto;
	}
	public IDetailPrepa[] getDetailsPrepa() {
		return DetailsPrepa;
	}
	public void setDetailsPrepa(IDetailPrepa[] detailsPrepa) {
		DetailsPrepa = detailsPrepa;
	}
	public IDetailMenu[] getDetailsMenu() {
		return DetailsMenu;
	}
	public void setDetailsMenu(IDetailMenu[] detailsMenu) {
		DetailsMenu = detailsMenu;
	}
	public IDetailStock[] getDetailsStock() {
		return DetailsStock;
	}
	public void setDetailsStock(IDetailStock[] detailsStock) {
		DetailsStock = detailsStock;
	}
	public String getCreationDate() {
		return CreationDate;
	}
	public void setCreationDate(String creationDate) {
		CreationDate = creationDate;
	}
	public String getLastModifDate() {
		return LastModifDate;
	}
	public void setLastModifDate(String lastModifDate) {
		LastModifDate = lastModifDate;
	}
	public int getLastModifStaff() {
		return LastModifStaff;
	}
	public void setLastModifStaff(int lastModifStaff) {
		LastModifStaff = lastModifStaff;
	}
	public boolean isBloque() {
		return Bloque;
	}
	public void setBloque(boolean bloque) {
		Bloque = bloque;
	}
	public boolean isPointe() {
		return Pointe;
	}
	public void setPointe(boolean pointe) {
		Pointe = pointe;
	}
	public boolean isDisabled() {
		return Disabled;
	}
	public void setDisabled(boolean disabled) {
		Disabled = disabled;
	}
	public boolean isPrixASaissir() {
		return PrixASaissir;
	}
	public void setPrixASaissir(boolean prixASaissir) {
		PrixASaissir = prixASaissir;
	}
	public boolean isNomASaissir() {
		return NomASaissir;
	}
	public void setNomASaissir(boolean nomASaissir) {
		NomASaissir = nomASaissir;
	}
	public boolean isOnlyResto() {
		return OnlyResto;
	}
	public void setOnlyResto(boolean onlyResto) {
		OnlyResto = onlyResto;
	}
	public boolean isOnlyEmport() {
		return OnlyEmport;
	}
	public void setOnlyEmport(boolean onlyEmport) {
		OnlyEmport = onlyEmport;
	}
	public boolean isForceEmport() {
		return ForceEmport;
	}
	public void setForceEmport(boolean forceEmport) {
		ForceEmport = forceEmport;
	}
	public boolean isPointageNoCumul() {
		return PointageNoCumul;
	}
	public void setPointageNoCumul(boolean pointageNoCumul) {
		PointageNoCumul = pointageNoCumul;
	}
	public boolean isForceResto() {
		return ForceResto;
	}
	public void setForceResto(boolean forceResto) {
		ForceResto = forceResto;
	}
	public boolean isInterditAZero() {
		return InterditAZero;
	}
	public void setInterditAZero(boolean interditAZero) {
		InterditAZero = interditAZero;
	}
	public boolean isInterditHorsMenu() {
		return InterditHorsMenu;
	}
	public void setInterditHorsMenu(boolean interditHorsMenu) {
		InterditHorsMenu = interditHorsMenu;
	}
	public boolean isInterditDansMenu() {
		return InterditDansMenu;
	}
	public void setInterditDansMenu(boolean interditDansMenu) {
		InterditDansMenu = interditDansMenu;
	}
	public boolean isOnlyOnceRemise() {
		return OnlyOnceRemise;
	}
	public void setOnlyOnceRemise(boolean onlyOnceRemise) {
		OnlyOnceRemise = onlyOnceRemise;
	}
	public boolean isHappyHourQty() {
		return HappyHourQty;
	}
	public void setHappyHourQty(boolean happyHourQty) {
		HappyHourQty = happyHourQty;
	}
	public boolean isNotInTotalDiscount() {
		return NotInTotalDiscount;
	}
	public void setNotInTotalDiscount(boolean notInTotalDiscount) {
		NotInTotalDiscount = notInTotalDiscount;
	}
	public boolean isNoChgPrix() {
		return NoChgPrix;
	}
	public void setNoChgPrix(boolean noChgPrix) {
		NoChgPrix = noChgPrix;
	}
	public boolean isNonFood() {
		return NonFood;
	}
	public void setNonFood(boolean nonFood) {
		NonFood = nonFood;
	}
	public boolean isControlINOUT() {
		return ControlINOUT;
	}
	public void setControlINOUT(boolean controlINOUT) {
		ControlINOUT = controlINOUT;
	}
	public boolean isMenuForfaitCandidat() {
		return MenuForfaitCandidat;
	}
	public void setMenuForfaitCandidat(boolean menuForfaitCandidat) {
		MenuForfaitCandidat = menuForfaitCandidat;
	}
	public boolean isKeepStock() {
		return KeepStock;
	}
	public void setKeepStock(boolean keepStock) {
		KeepStock = keepStock;
	}
	public boolean isListeReassort() {
		return ListeReassort;
	}
	public void setListeReassort(boolean listeReassort) {
		ListeReassort = listeReassort;
	}
	public boolean isBlocageAuto() {
		return BlocageAuto;
	}
	public void setBlocageAuto(boolean blocageAuto) {
		BlocageAuto = blocageAuto;
	}
	public boolean isAuCentimetre() {
		return AuCentimetre;
	}
	public void setAuCentimetre(boolean auCentimetre) {
		AuCentimetre = auCentimetre;
	}
	public boolean isNoDetailsIfResto() {
		return NoDetailsIfResto;
	}
	public void setNoDetailsIfResto(boolean noDetailsIfResto) {
		NoDetailsIfResto = noDetailsIfResto;
	}
	public boolean isNoDetailsIfEmport() {
		return NoDetailsIfEmport;
	}
	public void setNoDetailsIfEmport(boolean noDetailsIfEmport) {
		NoDetailsIfEmport = noDetailsIfEmport;
	}
	public boolean isOnlyManager() {
		return OnlyManager;
	}
	public void setOnlyManager(boolean onlyManager) {
		OnlyManager = onlyManager;
	}
	public boolean isBalance() {
		return Balance;
	}
	public void setBalance(boolean balance) {
		Balance = balance;
	}
	public boolean isExternal() {
		return External;
	}
	public void setExternal(boolean external) {
		External = external;
	}
	public boolean isAdditionNoCumul() {
		return AdditionNoCumul;
	}
	public void setAdditionNoCumul(boolean additionNoCumul) {
		AdditionNoCumul = additionNoCumul;
	}
	public boolean isNoTicketIfEmport() {
		return NoTicketIfEmport;
	}
	public void setNoTicketIfEmport(boolean noTicketIfEmport) {
		NoTicketIfEmport = noTicketIfEmport;
	}
	public boolean isKeepStockOnlyIngredients() {
		return KeepStockOnlyIngredients;
	}
	public void setKeepStockOnlyIngredients(boolean keepStockOnlyIngredients) {
		KeepStockOnlyIngredients = keepStockOnlyIngredients;
	}
	public boolean isStayInDepartement() {
		return StayInDepartement;
	}
	public void setStayInDepartement(boolean stayInDepartement) {
		StayInDepartement = stayInDepartement;
	}
	public boolean isForceDetailLinked() {
		return ForceDetailLinked;
	}
	public void setForceDetailLinked(boolean forceDetailLinked) {
		ForceDetailLinked = forceDetailLinked;
	}
	public boolean isPreparationOpen() {
		return PreparationOpen;
	}
	public void setPreparationOpen(boolean preparationOpen) {
		PreparationOpen = preparationOpen;
	}
	public boolean isPreparationControled() {
		return PreparationControled;
	}
	public void setPreparationControled(boolean preparationControled) {
		PreparationControled = preparationControled;
	}
	public boolean isPreparationGrouped() {
		return PreparationGrouped;
	}
	public void setPreparationGrouped(boolean preparationGrouped) {
		PreparationGrouped = preparationGrouped;
	}
	public boolean isPreparationShowNivDescr() {
		return PreparationShowNivDescr;
	}
	public void setPreparationShowNivDescr(boolean preparationShowNivDescr) {
		PreparationShowNivDescr = preparationShowNivDescr;
	}
	public boolean isPreparationSortByNom() {
		return PreparationSortByNom;
	}
	public void setPreparationSortByNom(boolean preparationSortByNom) {
		PreparationSortByNom = preparationSortByNom;
	}
	public boolean isNeedDrapeau4Lancer() {
		return NeedDrapeau4Lancer;
	}
	public void setNeedDrapeau4Lancer(boolean needDrapeau4Lancer) {
		NeedDrapeau4Lancer = needDrapeau4Lancer;
	}
	public boolean isNeedDrapeau4Add() {
		return NeedDrapeau4Add;
	}
	public void setNeedDrapeau4Add(boolean needDrapeau4Add) {
		NeedDrapeau4Add = needDrapeau4Add;
	}
	public boolean isRptModifFlag1() {
		return RptModifFlag1;
	}
	public void setRptModifFlag1(boolean rptModifFlag1) {
		RptModifFlag1 = rptModifFlag1;
	}
	public boolean isRptModifFlag2() {
		return RptModifFlag2;
	}
	public void setRptModifFlag2(boolean rptModifFlag2) {
		RptModifFlag2 = rptModifFlag2;
	}
	public boolean isRptModifFlag3() {
		return RptModifFlag3;
	}
	public void setRptModifFlag3(boolean rptModifFlag3) {
		RptModifFlag3 = rptModifFlag3;
	}
	public boolean isRptModifFlag4() {
		return RptModifFlag4;
	}
	public void setRptModifFlag4(boolean rptModifFlag4) {
		RptModifFlag4 = rptModifFlag4;
	}
	
	
	

}
