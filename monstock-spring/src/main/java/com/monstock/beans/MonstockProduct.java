package com.monstock.beans;

import java.io.IOException;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class MonstockProduct {
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public boolean isProductoreqpt() {
		return productoreqpt;
	}



	public void setProductoreqpt(boolean productoreqpt) {
		this.productoreqpt = productoreqpt;
	}



	public String getReference_company() {
		return reference_company;
	}



	public void setReference_company(String reference_company) {
		this.reference_company = reference_company;
	}



	public String getLabel() {
		return label;
	}



	public void setLabel(String label) {
		this.label = label;
	}



	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public String getCode_gentype() {
		return code_gentype;
	}



	public void setCode_gentype(String code_gentype) {
		this.code_gentype = code_gentype;
	}



	public String[] getCategory() {
		return category;
	}



	public void setCategory(String[] category) {
		this.category = category;
	}



	public String[] getTags() {
		return tags;
	}



	public void setTags(String[] tags) {
		this.tags = tags;
	}



	public String getSize() {
		return size;
	}



	public void setSize(String size) {
		this.size = size;
	}



	public String getColor() {
		return color;
	}



	public void setColor(String color) {
		this.color = color;
	}



	public String getReference_provider() {
		return reference_provider;
	}



	public void setReference_provider(String reference_provider) {
		this.reference_provider = reference_provider;
	}



	public String getStatut() {
		return statut;
	}



	public void setStatut(String statut) {
		this.statut = statut;
	}



	public String getDimlenght() {
		return dimlenght;
	}



	public void setDimlenght(String dimlenght) {
		this.dimlenght = dimlenght;
	}



	public String getDimwidth() {
		return dimwidth;
	}



	public void setDimwidth(String dimwidth) {
		this.dimwidth = dimwidth;
	}



	public String getDimtall() {
		return dimtall;
	}



	public void setDimtall(String dimtall) {
		this.dimtall = dimtall;
	}



	public String getWeight() {
		return weight;
	}



	public void setWeight(String weight) {
		this.weight = weight;
	}



	public String getUnit() {
		return unit;
	}



	public void setUnit(String unit) {
		this.unit = unit;
	}



	public String getPerishable_duration() {
		return perishable_duration;
	}



	public void setPerishable_duration(String perishable_duration) {
		this.perishable_duration = perishable_duration;
	}



	public boolean isDangerous() {
		return dangerous;
	}



	public void setDangerous(boolean dangerous) {
		this.dangerous = dangerous;
	}



	public String getMaker() {
		return maker;
	}



	public void setMaker(String maker) {
		this.maker = maker;
	}



	public String[] getSupplier() {
		return supplier;
	}



	public void setSupplier(String[] supplier) {
		this.supplier = supplier;
	}



	public String getGps() {
		return gps;
	}



	public void setGps(String gps) {
		this.gps = gps;
	}



	public String getVariation() {
		return variation;
	}



	public void setVariation(String variation) {
		this.variation = variation;
	}



	public int getVariation_mother() {
		return variation_mother;
	}



	public void setVariation_mother(int variation_mother) {
		this.variation_mother = variation_mother;
	}



	public String[] getVariation_carac() {
		return variation_carac;
	}



	public void setVariation_carac(String[] variation_carac) {
		this.variation_carac = variation_carac;
	}



	public String getInternal_code() {
		return internal_code;
	}



	public void setInternal_code(String internal_code) {
		this.internal_code = internal_code;
	}



	public Date getDate_purchase() {
		return date_purchase;
	}



	public void setDate_purchase(Date date_purchase) {
		this.date_purchase = date_purchase;
	}



	public Date getDate_control() {
		return date_control;
	}



	public void setDate_control(Date date_control) {
		this.date_control = date_control;
	}



	public boolean isArchive() {
		return archive;
	}



	public void setArchive(boolean archive) {
		this.archive = archive;
	}



	public Date getCreated() {
		return created;
	}



	public void setCreated(Date created) {
		this.created = created;
	}



	public Date getModified() {
		return modified;
	}



	public void setModified(Date modified) {
		this.modified = modified;
	}



	public int getCompany_id() {
		return company_id;
	}



	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}



	public int getEnv_id() {
		return env_id;
	}



	public void setEnv_id(int env_id) {
		this.env_id = env_id;
	}



	public String getUser_num_1() {
		return user_num_1;
	}



	public void setUser_num_1(String user_num_1) {
		this.user_num_1 = user_num_1;
	}



	public String getUser_num_2() {
		return user_num_2;
	}



	public void setUser_num_2(String user_num_2) {
		this.user_num_2 = user_num_2;
	}



	public String getUser_num_3() {
		return user_num_3;
	}



	public void setUser_num_3(String user_num_3) {
		this.user_num_3 = user_num_3;
	}



	public String getUser_num_4() {
		return user_num_4;
	}



	public void setUser_num_4(String user_num_4) {
		this.user_num_4 = user_num_4;
	}



	public String getUser_num_5() {
		return user_num_5;
	}



	public void setUser_num_5(String user_num_5) {
		this.user_num_5 = user_num_5;
	}



	public Date getUser_date_1() {
		return user_date_1;
	}



	public void setUser_date_1(Date user_date_1) {
		this.user_date_1 = user_date_1;
	}



	public Date getUser_date_2() {
		return user_date_2;
	}



	public void setUser_date_2(Date user_date_2) {
		this.user_date_2 = user_date_2;
	}



	public Date getUser_date_3() {
		return user_date_3;
	}



	public void setUser_date_3(Date user_date_3) {
		this.user_date_3 = user_date_3;
	}



	public Date getUser_date_4() {
		return user_date_4;
	}



	public void setUser_date_4(Date user_date_4) {
		this.user_date_4 = user_date_4;
	}



	public Date getUser_date_5() {
		return user_date_5;
	}



	public void setUser_date_5(Date user_date_5) {
		this.user_date_5 = user_date_5;
	}



	public String getUser_text_1() {
		return user_text_1;
	}



	public void setUser_text_1(String user_text_1) {
		this.user_text_1 = user_text_1;
	}



	public String getUser_text_2() {
		return user_text_2;
	}



	public void setUser_text_2(String user_text_2) {
		this.user_text_2 = user_text_2;
	}



	public String getUser_text_3() {
		return user_text_3;
	}



	public void setUser_text_3(String user_text_3) {
		this.user_text_3 = user_text_3;
	}



	public String getUser_text_4() {
		return user_text_4;
	}



	public void setUser_text_4(String user_text_4) {
		this.user_text_4 = user_text_4;
	}



	public String getUser_text_5() {
		return user_text_5;
	}



	public void setUser_text_5(String user_text_5) {
		this.user_text_5 = user_text_5;
	}



	public boolean isUser_boo_1() {
		return user_boo_1;
	}



	public void setUser_boo_1(boolean user_boo_1) {
		this.user_boo_1 = user_boo_1;
	}



	public boolean isUser_boo_2() {
		return user_boo_2;
	}



	public void setUser_boo_2(boolean user_boo_2) {
		this.user_boo_2 = user_boo_2;
	}



	public boolean isUser_boo_3() {
		return user_boo_3;
	}



	public void setUser_boo_3(boolean user_boo_3) {
		this.user_boo_3 = user_boo_3;
	}



	public boolean isUser_boo_4() {
		return user_boo_4;
	}



	public void setUser_boo_4(boolean user_boo_4) {
		this.user_boo_4 = user_boo_4;
	}



	public boolean isUser_boo_5() {
		return user_boo_5;
	}



	public void setUser_boo_5(boolean user_boo_5) {
		this.user_boo_5 = user_boo_5;
	}



	public boolean isTracability() {
		return tracability;
	}



	public void setTracability(boolean tracability) {
		this.tracability = tracability;
	}



	//"data": {
    private int id; //": 0,
    private boolean productoreqpt;//": true,
    private String reference_company; //": "string",
    private String label;//": "string",
    private String type; //": "string",
    private String description; //": "string",
    private String code; //": "string",
    private String code_gentype; //": "string",
    private String[] category; //": [
     // "string"
    //],
    private String[] tags; //": [
      //"string"
    //],
    private String size; //": "string",
    private String color; //": "string",
    private String reference_provider; //": "string",
    private String statut; //": "string",
    private String dimlenght; //": "string",
    private String dimwidth; //": "string",
    private String dimtall; //": "string",
    private String weight; //": "string",
    private String unit; //": "string",
    private String perishable_duration; //": "string",
    private boolean dangerous; //": true,
    private String maker; //": "string",
    private String[] supplier; //": [
      //"string"
    //],
    private String gps; //": "string",
    private String variation; //": "string",
    private int variation_mother; //": 0,
    private String[] variation_carac; //": [
      //"string"
    //],
    private String internal_code; //": "string",
    private Date date_purchase; //": "2019-07-10T03:08:24.569Z",
    private Date date_control; //": "2019-07-10T03:08:24.569Z",
    private boolean archive; //": true,
    private Date created; //": "2019-07-10T03:08:24.569Z",
    private Date modified; //": "2019-07-10T03:08:24.569Z",
    private int company_id; //": 0,
    private int env_id; //": 0,
    private String user_num_1; //": "string",
    private String user_num_2; //": "string",
    private String user_num_3; //": "string",
    private String user_num_4; //": "string",
    private String user_num_5; //": "string",
    private Date user_date_1; //": "2019-07-10T03:08:24.569Z",
    private Date user_date_2; //": "2019-07-10T03:08:24.569Z",
    private Date user_date_3; //": "2019-07-10T03:08:24.569Z",
    private Date user_date_4; //": "2019-07-10T03:08:24.569Z",
    private Date user_date_5; //": "2019-07-10T03:08:24.569Z",
    private String user_text_1; //": "string",
    private String user_text_2; //": "string",
    private String user_text_3; //": "string",
    private String user_text_4; //": "string",
    private String user_text_5; //": "string",
    private boolean user_boo_1; //": true,
    private boolean user_boo_2; //": true,
    private boolean user_boo_3; //": true,
    private boolean user_boo_4; //": true,
    private boolean user_boo_5; //": true,
    private boolean tracability; //": true */
    
	
	
    public static String map(String anArticle) throws JsonParseException, JsonMappingException,IOException{
    	
    	ObjectMapper mapper = new ObjectMapper();
    	mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    	mapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
    	RestoMaxArticle rmArticle =  mapper.readValue(anArticle,RestoMaxArticle.class);
    	MonstockProduct msProduct = new MonstockProduct();
    	msProduct.setId(Integer.parseInt(rmArticle.getPLU()));
    	msProduct.setCode( Integer.toString( rmArticle.getType()));
    	msProduct.setDescription(rmArticle.getTypeDescr());
    	msProduct.setWeight(Integer.toString(rmArticle.getFlags()));
    	
    	return mapper.writeValueAsString(msProduct);
    	
    }

}
